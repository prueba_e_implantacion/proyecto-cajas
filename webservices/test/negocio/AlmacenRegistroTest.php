<?php

require_once dirname(__FILE__) . '/../../negocio/AlmacenRegistro.php';
include dirname(__FILE__).'/../../simpletest/autorun.php';

class TestAlmacenRegistro extends UnitTestCase {

   public function testAgregar() {
       $obj = new AlmacenRegistro();

       $obj->setContacto("TEST CONTACTO");
       $obj->setCantidad("100");
       $obj->setObservaciones("TEST OBSERVACION");
       $obj->setOperacion("H");
       $obj->setId_caja("25");
       $obj->setId_usuario_area("1");
       $obj->setId_ubicacion("35");
       $obj->setId_cliente("313");

       $this->assertTrue($obj->agregar());
   }

   public function testRetirar() {
       $obj = new AlmacenRegistro();

       $obj->setId_registro(1);
       $obj->setContacto("TEST CONTACTO");
       $obj->setCantidad("100");
       $obj->setObservaciones("TEST OBSERVACION");
       $obj->setOperacion("S");
       $obj->setId_usuario_area("1");

       $this->assertTrue($obj->ingresar_retirar());
   }

}