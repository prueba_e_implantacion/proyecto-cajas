<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/AlmacenRegistro.php';
require_once '../util/funciones/Funciones.clase.php';

$tabla = $_POST["p_tabla"];
$id_user = $_POST["p_id_user"];
$id = $_POST["p_id"];
$nro_placa = $_POST["p_nro_placa"];

try {

    $obj = new AlmacenRegistro();

    $resultado = $obj->anularRegistro($tabla, $id_user, $id, $nro_placa, $nro_placa);

    if ($resultado) {
        Funciones::imprimeJSON(200, "Registro Satisfactorio", "");
    } else {
        Funciones::imprimeJSON(500, $exc->getMessage(), "");
    }
} catch (Exception $exc) {
    //Funciones::imprimeJSON(500, $exc->getMessage(), "");
    echo $exc->getMessage();
}
